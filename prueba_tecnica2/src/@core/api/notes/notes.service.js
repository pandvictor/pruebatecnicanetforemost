import { Get, Post, Put } from "../httpService";

//const API_URL = process.env.API_URL
export async function getAllNotes() {
  return await Get("getnotes")
    .then((response) => {
      const { data, statusCode } = response.data;

      if (statusCode === 200) {
        return { status: true, notes: data.notes };
      }

      return { status: false, message: data.message };
    })
    .catch((error) => {
      console.log(error);

      return { status: false, message: "Error de conexión!" };
    });
}
export async function addNote(item) {
  return await Put("addnote", { ...item })
    .then((response) => {
      const { data, statusCode } = response.data;

      if (statusCode === 200) {
        return { status: true };
      }

      return { status: false, message: data.message };
    })
    .catch((error) => {
      console.log(error);

      return { status: false, message: "Error de conexión!" };
    });
}
