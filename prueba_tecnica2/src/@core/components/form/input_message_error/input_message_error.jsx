import React from "react";

const InputMessageError = ({ error }) => {
  return (
    <>{error && <div className='invalid-feedback d-block'>{error}</div>}</>
  );
};

export default InputMessageError;
