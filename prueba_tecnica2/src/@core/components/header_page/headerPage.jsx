import { React } from "react";
import logo from "./logo.svg";
const HeaderPage = () => {
  return (
    <header className='App-header'>
      <div className='d-flex flex-column'>
        <div>
          <img
            src={logo}
            style={{ width: "64px" }}
            className='App-logo'
            alt='logo'
          />
        </div>
        <div>Prueba Tecnica 2</div>
        <div>Victor Alejandro Hernandez Gomez</div>
      </div>
    </header>
  );
};

export default HeaderPage;
