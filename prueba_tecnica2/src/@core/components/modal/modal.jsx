import React from "react";

const Modal = (props) => {
  const { show, handleClose } = props;
  return (
    <div
      className={`modal ${show ? "d-block" : "d-none"}`}
      tabIndex={-1}
      role='dialog'>
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title'></h5>
            <button
              type='button'
              className='close'
              onClick={handleClose}
              aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div className='modal-body'>{props.children}</div>
          <div className='modal-footer'>
            <button
              type='button'
              className='btn btn-secondary'
              onClick={handleClose}>
              Cerrar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
