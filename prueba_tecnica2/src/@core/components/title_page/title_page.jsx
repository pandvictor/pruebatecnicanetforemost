import React from "react";

const TitlePage = ({ title }) => {
  return (
    <div className='card mt-3'>
      <div className='card-body'>
        <h5 className='card-title'>{title}</h5>
      </div>
    </div>
  );
};

export default TitlePage;
