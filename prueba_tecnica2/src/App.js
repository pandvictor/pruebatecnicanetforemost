import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import HomePage from "./components/pages/home";

function App() {
  return <HomePage />;
}

export default App;
