import React, { useState, useEffect } from "react";
import { addNote, getAllNotes } from "../../../@core/api/notes/notes.service";

import HeaderPage from "../../../@core/components/header_page/headerPage";
import Modal from "../../../@core/components/modal/modal";
import CreateNote from "../../views/create_note/create_note";
import ListNotes from "../../views/list_notes/list_notes";

const HomePage = () => {
  const [listItems, setListItems] = useState([]);
  const [showCreate, setShowCreate] = useState(false);
  const [editItem, setEditItem] = useState();

  const getAllRemoteNotes = async () => {
    const { status, notes } = await getAllNotes();
    if (status) setListItems(notes);
  };

  useEffect(() => {
    getAllRemoteNotes();
  }, []);

  async function handleAddNewItem(item) {
    let newList = [...listItems];
    newList.push(item);
    setListItems(newList);
    await addNote(item);
    setShowCreate(false);
  }
  function handleEditButton(item) {
    setEditItem(item);
    setShowCreate(true);
  }
  return (
    <div className='App'>
      <HeaderPage />
      <div className='mt-2'>
        <button
          type='button'
          className='btn btn-primary'
          onClick={() => setShowCreate(true)}>
          Crear nuevo
        </button>
      </div>
      <Modal show={showCreate} handleClose={() => setShowCreate(false)}>
        <CreateNote handleAddNewItem={handleAddNewItem} editItem={editItem} />
      </Modal>
      <ListNotes notes={listItems} editButton={handleEditButton} />
    </div>
  );
};

export default HomePage;
