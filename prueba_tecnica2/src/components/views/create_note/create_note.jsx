import React, { useState, useEffect } from "react";
import InputMessageError from "../../../@core/components/form/input_message_error/input_message_error";
import TitlePage from "../../../@core/components/title_page/title_page";

const CreateNote = ({ editItem, handleAddNewItem }) => {
  const [titulo, setTitulo] = useState("");
  const [cuerpo, setCuerpo] = useState("");
  const [fecha, setFecha] = useState("");

  const [errors, setErrors] = useState({
    titulo: "",
    cuerpo: "",
    fecha: "",
  });

  useEffect(() => {
    if (editItem) {
      setTitulo(editItem.titulo);
      setCuerpo(editItem.cuerpo);
      setFecha(editItem.fecha);
    }
  }, [editItem]);

  const handleSubmit = (event) => {
    event.preventDefault();
    let newErrors = {
      titulo: "",
      cuerpo: "",
      fecha: "",
    };
    if (!titulo) newErrors.titulo = "Titulo no puede ser vacio.";
    if (!cuerpo) newErrors.cuerpo = "Cuerpo no puede ser vacio.";
    if (!fecha) newErrors.fecha = "Fecha no puede ser vacio.";

    setErrors({ ...newErrors });
    if (titulo && cuerpo && fecha) handleAddNewItem({ titulo, cuerpo, fecha });
  };

  return (
    <div className='p-1'>
      <TitlePage title={"Crear un nuevo Item"} />
      <form className='mt-4' onSubmit={(e) => e.preventDefault()}>
        <div className='form-group row'>
          <label className='col-sm-2 col-form-label'>Titulo</label>
          <div className='col-sm-10'>
            <input
              type='text'
              className='form-control'
              id='titulo'
              value={titulo}
              onChange={(e) => setTitulo(e.target.value)}
            />
            <InputMessageError error={errors.titulo} />
          </div>
        </div>
        <div className='form-group row mt-1'>
          <label className='col-sm-2 col-form-label'>Cuerpo</label>
          <div className='col-sm-10'>
            <textarea
              type='text'
              className='form-control'
              id='cuerpo'
              value={cuerpo}
              onChange={(e) => setCuerpo(e.target.value)}
            />
            <InputMessageError error={errors.cuerpo} />
          </div>
        </div>
        <div className='form-group row mt-1'>
          <label className='col-sm-2 col-form-label'>Fecha</label>
          <div className='col-sm-10'>
            <input
              type='date'
              className='form-control'
              id='fecha'
              value={fecha}
              onChange={(e) => setFecha(e.target.value)}
            />
            <InputMessageError error={errors.fecha} />
          </div>
        </div>
        <button
          type='submit'
          onClick={handleSubmit}
          className='btn btn-primary'>
          Guardar
        </button>
      </form>
    </div>
  );
};

export default CreateNote;
