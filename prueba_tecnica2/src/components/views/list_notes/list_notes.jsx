import React from "react";
import { useState, useEffect } from "react";

const NoteItem = ({ note, editButton }) => {
  return (
    <tr key={note.titulo}>
      <td>{note.titulo}</td>
      <td>{note.cuerpo}</td>
      <td>{note.fecha}</td>
      <td>
        <button
          type='button'
          className='btn btn-success'
          onClick={() => editButton(note)}>
          Editar
        </button>
      </td>
    </tr>
  );
};

const ListNotes = ({ notes, editButton }) => {
  const [itemsList, setItemsList] = useState(null);
  const [searchTitleText, setSearchTitleText] = useState("");
  const [searchCuerpoText, setSearchCuerpoText] = useState("");
  const [searchFechaText, setSearchFechaText] = useState("");

  useEffect(() => {
    if (notes) setItemsList(notes);
  }, [notes]);

  useEffect(() => {
    let newList = [...notes];
    if (searchTitleText)
      newList = newList.filter((e) => e.titulo.includes(searchTitleText));

    if (searchCuerpoText)
      newList = newList.filter((e) => e.cuerpo.includes(searchCuerpoText));

    if (searchFechaText)
      newList = newList.filter((e) => e.fecha.includes(searchFechaText));
    setItemsList(newList);
  }, [searchTitleText, searchCuerpoText, searchFechaText]);
  return (
    <div className='mt-2 d-flex-row'>
      Notas
      <table>
        <thead>
          <tr>
            <th>Titulo</th>
            <th>Cuerpo</th>
            <th>Fecha</th>
          </tr>
        </thead>
        <tbody>
          <>
            <tr key={"search"}>
              <td>
                <input
                  name={"titleSearch"}
                  value={searchTitleText}
                  type='text'
                  onChange={(e) => setSearchTitleText(e.target.value)}
                />
              </td>
              <td>
                <input
                  name={"cuerpoSearch"}
                  value={searchCuerpoText}
                  onChange={(e) => {
                    setSearchCuerpoText(e.target.value);
                  }}
                  type='text'
                />
              </td>
              <td>
                <input
                  name={"fechaSearch"}
                  value={searchFechaText}
                  onChange={(e) => setSearchFechaText(e.target.value)}
                  type='date'
                />
              </td>
              <td></td>
            </tr>
          </>
          {itemsList &&
            itemsList.map((note, index) => (
              <NoteItem note={note} editButton={editButton} key={index} />
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListNotes;
